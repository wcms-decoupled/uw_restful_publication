<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Main Menu'),
  'resource' => 'publication_main_menu',
  'name' => 'publication_main_menu__1_0',
  'entity_type' => 'menu_link',
  'bundle' => 'main-menu',
  'description' => t('Export the publication main menu.'),
  'class' => 'RestfulPublicationMainMenuResource',
);
