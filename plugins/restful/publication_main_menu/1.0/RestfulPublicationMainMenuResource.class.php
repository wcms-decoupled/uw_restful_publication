<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationMainMenuResource extends RestfulEntityBaseMenu {

  /**
   * Overrides RestfulEntityBaseMenu::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    // @code
    // $public_fields['alias'] = array(
    //   'callback' => array($this, 'getEntityAlias'),
    // );
    // $public_fields['source'] = array(
    //   'callback' => array($this, 'getEntitySource'),
    // );
    // $public_fields['name'] = array(
    //   'property' => 'name',
    // );
    // @endcode
    return $public_fields;
  }

}
