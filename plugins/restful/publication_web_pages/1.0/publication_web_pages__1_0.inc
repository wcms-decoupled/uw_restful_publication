<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Web Page'),
  'resource' => 'publication_web_pages',
  'name' => 'publication_web_pages__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_web_page',
  'description' => t('Export the publication web pages content type.'),
  'class' => 'RestfulPublicationWebPagesResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
