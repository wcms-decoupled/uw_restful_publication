<?php

/**
 * @file
 */
class RestfulPublicationWebPagesResource extends RestfulEntityBaseNodeExtended {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['title'] = array(
      'property' => 'title_field',
    );
    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'value',
    );
    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
    );
    $public_fields['image'] = array(
      'property' => 'field_image',
    );
    $public_fields['file'] = array(
      'property' => 'field_file',
    );
    $public_fields['audience'] = array(
      'property' => 'field_audience',
    );
    $public_fields['moderation_revision'] = array(
      'property' => 'nid',
      'access_callbacks' => array(
        array($this, 'accessModeration'),
      ),
      'process_callbacks' => array(
        array($this, 'getModerationRevision'),
      )
    );
    return $public_fields;
  }
  /**
   * Get entity's current revision if not published.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array or NULL
   *   The entity revision resource.
   */
  protected function getModerationRevision($nid) {
    $revision_bundle = 'publication_web_pages_revision';
    $entity = entity_load_single('node', $nid);
    if ($entity->workbench_moderation['current']->state == 'published') {
      return;
    }
    else {
      $handler = restful_get_restful_handler($revision_bundle);
      $result = $handler->get($nid);
      return $result;
    }
  }
  /**
   * Overrides parent filter the query for list.
   *
   * @param \entityfieldquery $query
   *   The query object.
   *
   * @see \restfulentitybase::getqueryforlist
   */
  protected function queryforlistfilter(\entityfieldquery $query) {
    foreach ($this->parserequestforlistfilter() as $filter) {
      // Map alias to the node id.
      if ($filter['public_field'] == 'alias') {
        $node_path = explode('/', drupal_get_normal_path($filter['value'][0]));
        $nid = $node_path[1];
        $query->entityCondition('entity_id', $nid, $filter['operator'][0]);
        // Unset the alias filter to prevent problems when we call parent function.
        unset($this->request['filter']['alias']);
      }
      // TODO Need to check if we need a categories filter for web pages.
      elseif ($filter['public_field'] == 'categories' && !is_numeric($filter['value'][0])) {
        // Category is the url friend machine name.  Replace it with its tid.
        $request = $this->getRequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_categories');
        $request['filter']['categories'] = $term->tid;
        $this->setRequest($request);
      }
    }
    parent::queryforlistfilter($query);;
  }
}
