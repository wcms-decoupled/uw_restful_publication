<?php

/**
 * @file
 * Contains RestfulEntityBaseMenu.
 */

/**
 * A base implementation for "Menu" entity type.
 */
class RestfulEntityBaseMenu extends RestfulEntityBase {

  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by 'weight' property in ascending order.
    return array('weight' => 'ASC');
  }

  /**
   * Overrides \RestfulEntityBase::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['weight'] = array(
      'property' => 'weight',
    );
    $public_fields['path'] = array(
      'property' => 'link_path',
    );
    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );
    $public_fields['publish_status'] = array(
      'callback' => array($this, 'getEntityPublishStatus'),
    );
    $public_fields['external_link'] = array(
      'callback' => array($this, 'getExternalLinkStatus'),
    );
    return $public_fields;
  }

  /**
   * Check if the link is external.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return booleans
   *   If link is external
   */
  protected function getExternalLinkStatus(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return url_is_external($values->link_path);
  }

  /**
   * Get node publish status.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return booleans
   *   When the node is published and workbench moderation is current and the
   *   current is published, it is true.
   */
  protected function getEntityPublishStatus(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    $nid = $this->getNodeFromAlias($values->link_path);
    if (!$nid) {
        
      // Special case for archives as its a view.
      if ($values->link_path == 'archives') {
        return TRUE;
      }
      // Special case if its a taxonomy page.
      $term = menu_get_object('taxonomy_term', 2, $values->link_path);
      if ($term) {
        return TRUE;
      }
      return FALSE;
    }
    else {
      $node = node_load($nid);
      $is_current = $node->workbench_moderation['current']->is_current;
      $is_current_published = $node->workbench_moderation['current']->published;
      if ($node->status == 1 && $is_current == 1 &&  $is_current_published == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    if ($values->link_path == '<front>') {
      return '';
    }
    else {
      return drupal_get_path_alias($values->link_path);
    }
  }

  /**
   * Get a node from an alias.
   *
   * @param string $alias
   *   The entity's node alias.
   *
   * @return int
   *   The node ID.
   */
  protected function getNodeFromAlias($alias) {
    if ($alias == '<front>') {
      $path = drupal_get_normal_path(variable_get('site_frontpage', 'node/1'));
    }
    else {
      $path = drupal_get_normal_path($alias);
    }
    if ($path) {
      $node = menu_get_object("node", 1, $path);
      if (!$node) {
        $path = drupal_lookup_path("source", $path);
        if ($path) {
          $node = menu_get_object("node", 1, $path);
          if ($node) {
            return $node->nid;
          }
        }
      }
      if ($node) {
        return $node->nid;
      }
    }
    return 0;
  }

  /**
   * Overrides RestfulEntityBase::getQueryForList().
   *
   * Expose only non-hidden/enabled links.
   */
  public function getQueryForList() {
    $query = parent::getQueryForList();
    $query->propertyCondition('hidden', 0);
    return $query;
  }

  /**
   * Overrides RestfulEntityBase::getQueryForList().
   *
   * Filter the unpublished internal links.
   */
  public function getList() {
    $items = parent::getList();
    $show_links = array();
    foreach ($items as $item) {
      if ($item['publish_status'] || $item['external_link']) {
        $show_links[] = $item;
      }
    }
    return $show_links;
  }

}
