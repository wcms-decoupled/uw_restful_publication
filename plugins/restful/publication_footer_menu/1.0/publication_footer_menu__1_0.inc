<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Footer Menu'),
  'resource' => 'publication_footer_menu',
  'name' => 'publication_footer_menu__1_0',
  'entity_type' => 'menu_link',
  'bundle' => 'menu-footer-menu',
  'description' => t('Export the publication footer menu.'),
  'class' => 'RestfulPublicationFooterMenuResource',
);
