<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Categories without Metadata'),
  'resource' => 'publication_categories_no_metadata',
  'name' => 'publication_categories_no_metadata__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_categories',
  'description' => t('Export the publication categories without metadata.'),
  'class' => 'RestfulPublicationCategoriesNoMetadataResource',
);
