<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Audiences'),
  'resource' => 'publication_audiences',
  'name' => 'publication_audiences__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uwaterloo_audience',
  'description' => t('Export the publication audiences.'),
  'class' => 'RestfulPublicationAudiencesResource',
);
