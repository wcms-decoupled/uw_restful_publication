<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Image Gallery'),
  'resource' => 'publication_image_gallery',
  'name' => 'publication_image_gallery__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_image_gallery',
  'description' => t('Export the uWaterloo image gallery content type.'),
  'class' => 'RestfulPublicationImageGalleryResource',
);
