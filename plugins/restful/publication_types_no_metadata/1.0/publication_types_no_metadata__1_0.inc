<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Types without Metadata'),
  'resource' => 'publication_types_no_metadata',
  'name' => 'publication_types_no_metadata__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_type',
  'description' => t('Export the publication types.'),
  'class' => 'RestfulPublicationTypesNoMetadataResource',
);
