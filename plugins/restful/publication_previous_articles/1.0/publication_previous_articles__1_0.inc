<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Previous Articles'),
  'resource' => 'publication_previous_articles',
  'name' => 'publication_previous_articles__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the publication previous articles.'),
  'class' => 'RestfulPublicationPreviousArticlesResource',
);
