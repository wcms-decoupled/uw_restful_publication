<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Themes without Metadata'),
  'resource' => 'publication_themes_no_metadata',
  'name' => 'publication_themes_no_metadata__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uwaterloo_theme',
  'description' => t('Export the publication themes without metadata.'),
  'class' => 'RestfulPublicationThemesNoMetadataResource',
);
