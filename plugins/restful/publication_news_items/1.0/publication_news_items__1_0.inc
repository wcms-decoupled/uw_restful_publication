<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication News Items'),
  'resource' => 'publication_news_items',
  'name' => 'publication_news_items__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_news_item',
  'description' => t('Export the uWaterloo news items content type.'),
  'class' => 'RestfulPublicationNewsItemsResource',
);
