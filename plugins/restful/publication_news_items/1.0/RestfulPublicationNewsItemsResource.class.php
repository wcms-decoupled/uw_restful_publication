<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationNewsItemsResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['title'] = array(
      'property' => 'title_field',
    );
    $public_fields['date'] = array(
      'property' => 'field_news_date',
    );
    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'value',
    );
    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
    );
    $public_fields['feature_image'] = array(
      'property' => 'field_news_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'medium', 'large', 'size540x370'),
    );
    $public_fields['image'] = array(
      'property' => 'field_image',
    );
    $public_fields['file'] = array(
      'property' => 'field_file',
    );
    $public_fields['audience'] = array(
      'property' => 'field_audience',
    );
    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    return $public_fields;
  }
  /**
   * Get entity's metatags information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
    }
    return $return_array;
  }
  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    if (static::isArrayNumeric($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->imageProcess($item);
      }
      return $output;
    }
    return array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'styles' => $value['image_styles'],
    );
  }

}
