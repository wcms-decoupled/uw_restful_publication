<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Sub-Navigation Articles'),
  'resource' => 'publication_sub-navigation_articles',
  'name' => 'publication_sub-navigation_articles__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the publication sub-navigation articles.'),
  'class' => 'RestfulPublicationSubNavigationArticlesResource',
);
