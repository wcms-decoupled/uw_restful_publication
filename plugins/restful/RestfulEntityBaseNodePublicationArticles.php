<?php
/**
 * @file
 * Contains RestfulEntityBaseNodePublicationArticles.
 */
class RestfulEntityBaseNodePublicationArticles extends RestfulEntityBaseNodeExtended {
  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by 'order' field in ascending order.
    return array('order' => 'ASC');
  }

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['order'] = array(
      'property' => 'field_order',
    );
    $public_fields['headline'] = array(
      'property' => 'title_field',
    );
    $public_fields['sub_headline'] = array(
      'property' => 'field_publication_sub_headline',
    );
    $public_fields['issue'] = array(
      'property' => 'field_issue',
      'resource' => array(
        'publication_issue' => 'publication_issues',
      ),
    );
    $public_fields['author'] = array(
      'property' => 'field_author_collection',
    );
    $public_fields['feature_image'] = array(
      'property' => 'field_feature_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'medium', 'large', 'size540x370', 'cover_photo', 'cover_photo_x2'),
    );
    $public_fields['teaser'] = array(
      'property' => 'field_publication_teaser',
    );
    $public_fields['contents'] = array(
      'callback' => array($this, 'getContentSource'),
    );
    $public_fields['image'] = array(
      'property' => 'field_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'medium', 'large'),
    );
    $public_fields['file'] = array(
      'property' => 'field_file',
    );
    $public_fields['related_articles'] = array(
      'property' => 'field_related_articles',
      'resource' => array(
        'publication_article' => 'publication_related_articles',
      ),
    );
    $public_fields['nested_articles'] = array(
      'property' => 'field_nested_articles',
      'resource' => array(
        'nested_publication_articles' => 'publication_nested_articles',
      ),
    );
    $public_fields['sub-navigation_articles'] = array(
      'property' => 'field_subnavigation_articles',
      'resource' => array(
        'publication_article' => 'publication_sub-navigation_articles',
      ),
    );
    $public_fields['next_article'] = array(
      'property' => 'field_next_article',
      'resource' => array(
        'publication_article' => 'publication_next_articles',
      ),
    );
    $public_fields['previous_article'] = array(
      'property' => 'field_previous_article',
      'resource' => array(
        'publication_article' => 'publication_previous_articles',
      ),
    );
    $public_fields['category'] = array(
      'property' => 'field_publication_category',
      'sub_property' => 'name',
    );
    $public_fields['categories'] = array(
      'property' => 'field_publication_category',
      'resource' => array(
        'publication_categories' => 'publication_categories_no_metadata',
      ),
    );
    // Check if the taxonomy theme field exist for publication articles.
    // Should only exist for Report and Magazine type publications.
    if (field_info_instance('node', 'field_taxonomy_theme', 'publication_article')) {
      $public_fields['themes'] = array(
        'property' => 'field_taxonomy_theme',
        'resource' => array(
          'uwaterloo_theme' => 'publication_themes_no_metadata',
        ),
      );
    }
    else {
      // Do not remove field.  Return NULL.
      $public_fields['themes'] = array(
        'callback' => array($this, 'returnNull'),
      );
    }
    // $public_fields['audiences'] = array(
    //  'property' => 'field_audience',
    //  'resource' => array(
    //    'uwaterloo_audience' => 'publication_audiences',
    //  ),
    // );
    $public_fields['contains'] = array(
      'property' => 'field_publication_type',
      'resource' => array(
        'publication_type' => 'publication_types_no_metadata',
      ),
    );
    $public_fields['in_current_issue'] = array(
      'property' => 'field_issue',
      'sub_property' => 'name',
      'process_callbacks' => array(
        array($this, 'inCurrentIssue'),
      ),
    );
    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    $public_fields['moderation_revision'] = array(
      'property' => 'nid',
      'access_callbacks' => array(
        array($this, 'accessModeration'),
      ),
      'process_callbacks' => array(
        array($this, 'getModerationRevision'),
      )
    );
    return $public_fields;
  }

  /**
   * Return NULL so public field still displays in API.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return NULL
   */
  protected function returnNull(\EntityDrupalWrapper $wrapper) {
    return NULL;
  }

  /**
   * Get entity's current revision if not published.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array or NULL
   *   The entity revision resource.
   */
  protected function getModerationRevision($nid) {
    $revision_bundle = 'publication_articles_revision';
    $entity = entity_load_single('node', $nid);
    if ($entity->workbench_moderation['current']->state == 'published') {
      return;
    }
    else {
      $handler = restful_get_restful_handler($revision_bundle);
      $result = $handler->get($nid);
      return $result;
    }
  }

  /**
   * Process callback, check if the issue is the same as the current issue.
   *
   * @param array $value
   *   The current object.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected function inCurrentIssue($value) {
    return variable_get('publication_current_issue') == $value ? TRUE : FALSE;
  }

  /**
   * Process callback for formatting the content for formatting the content.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return array
   *   An array with keys external_source, external_link, and articleBody.
   */
  protected function getContentSource(\EntityDrupalWrapper $wrapper) {
    // Check if field collection exist.  using empty() throws errors.
    if ($fcid = $wrapper->field_publication_main_content->getIdentifier()) {
      if ($wrapper->field_publication_main_content->field_use_external_source->value()) {
        if ($link = $wrapper->field_publication_main_content->field_external_content_link->value()) {
          return array(
            'external_source' => $link['url'],
            'external_link' => TRUE,
            'articleBody' => NULL,
          );
        }
        else {
          return null;
        }
      } else {
        // The function value() returns the filtered article body.
        $article_body = $wrapper->field_publication_main_content->field_publication_content->value->value();

        return array(
          'external_source' => NULL,
          'external_link' => FALSE,
          'articleBody' => $article_body,
        );
      }
    }
    else {
      return null;
    }
  }

  /**
   * Overrides parent filter the query for list.
   *
   * @param \entityfieldquery $query
   *   The query object.
   *
   * @see \restfulentitybase::getqueryforlist
   */
  protected function queryforlistfilter(\entityfieldquery $query) {
    foreach ($this->parserequestforlistfilter() as $filter) {
      // map alias to the node id.
      if ($filter['public_field'] == 'alias') {
        // Do not need to do anything special anymore because we override the setPath function.
        // Unset the alias filter to prevent problems when we call parent function.
        unset($this->request['filter']['alias']);
      }
      elseif ($filter['public_field'] == 'issue') {
        // issue is the url friend machine name.  replace it with its tid.
        $request = $this->getrequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_issue');
        $request['filter']['issue'] = $term->tid;
        $this->setrequest($request);
      }
      elseif ($filter['public_field'] == 'categories') {
        // category is the url friend machine name.  replace it with its tid.
        $request = $this->getrequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_categories');
        $request['filter']['categories'] = $term->tid;
        $this->setrequest($request);
      }
      elseif ($filter['public_field'] == 'themes') {
        // category is the url friend machine name.  replace it with its tid.
        $request = $this->getrequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'uwaterloo_theme');
        $request['filter']['themes'] = $term->tid;
        $this->setrequest($request);
      }
      elseif ($filter['public_field'] == 'contains') {
        // category is the url friend machine name.  replace it with its tid.
        $request = $this->getrequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_type');
        $request['filter']['contains'] = $term->tid;
        $this->setrequest($request);
      }
      elseif ($filter['public_field'] == 'in_current_issue' && $filter['value'][0] == 'true') {
        // set the filter to the current issue tid.
        $request = $this->getrequest();
        $current_issue_name = variable_get('publication_current_issue');
        $term = taxonomy_get_term_by_name($current_issue_name, 'publication_issue');
        $request['filter']['in_current_issue'] = array_shift($term)->tid;
        $this->setrequest($request);
      }
    }
    parent::queryforlistfilter($query);;
  }

}
