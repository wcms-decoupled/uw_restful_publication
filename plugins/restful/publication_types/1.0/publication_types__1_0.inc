<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Types'),
  'resource' => 'publication_types',
  'name' => 'publication_types__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_type',
  'description' => t('Export the publication types without metadata.'),
  'class' => 'RestfulPublicationTypesResource',
);
