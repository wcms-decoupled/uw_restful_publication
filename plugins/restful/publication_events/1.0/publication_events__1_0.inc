<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Events'),
  'resource' => 'publication_events',
  'name' => 'publication_events__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_event',
  'description' => t('Export the uWaterloo events content type.'),
  'class' => 'RestfulPublicationEventsResource',
);
