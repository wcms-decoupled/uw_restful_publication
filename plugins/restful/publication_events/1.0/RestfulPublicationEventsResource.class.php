<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationEventsResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['name'] = array(
      'property' => 'title_field',
    );
    // @todo reformat date.
    $public_fields['date'] = array(
      'property' => 'field_event_date',
    );
    $public_fields['listing_image'] = array(
      'property' => 'field_event_image',
    );
    $public_fields['description'] = array(
      'property' => 'body',
      'sub_property' => 'value',
    );
    $public_fields['image'] = array(
      'property' => 'field_image',
    );
    $public_fields['file'] = array(
      'property' => 'field_file',
    );
    $public_fields['host'] = array(
      'property' => 'field_event_host_link',
    );
    $public_fields['website'] = array(
      'property' => 'field_event_moreinfo_link',
    );
    $public_fields['cost'] = array(
      'property' => 'field_event_cost',
    );
    // @todo Fix location. Currently unknown data property.
    // @code
    // $public_fields['location'] = array(
    //   'property' => 'field_event_location',
    // );
    // @endcode
    $public_fields['map'] = array(
      'property' => 'field_event_map_link',
    );
    $public_fields['audience'] = array(
      'property' => 'field_audience',
    );
    $public_fields['type'] = array(
      'property' => 'field_uw_event_type',
    );
    $public_fields['tags'] = array(
      'property' => 'field_uw_event_tag',
    );
    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    return $public_fields;
  }

  /**
   * Get entity's metatags information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
    }
    return $return_array;
  }

}
