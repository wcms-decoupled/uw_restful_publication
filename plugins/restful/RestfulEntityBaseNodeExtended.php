<?php
/**
 * @file
 * Extends the RestfulEntityBaseNode class.
 */
class RestfulEntityBaseNodeExtended extends RestfulEntityBaseNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );
    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );
    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      )
    );
    $public_fields['display_wide_screen'] = array(
      'property' => 'uw_page_settings_node',
    );
    $public_fields['moderation_tabs'] = array(
      'property' => 'nid',
      'access_callbacks' => array(
        array($this, 'accessModeration'),
      ),
      'process_callbacks' => array(
        array($this, 'getModerationTabs'),
      )
    );
    $public_fields['moderation_block'] = array(
      'property' => 'nid',
      'access_callbacks' => array(
        array($this, 'accessModeration'),
      ),
//      'callback' =>  array($this, 'getModerationBlock'),
      'process_callbacks' => array(
        array($this, 'getModerationBlock'),
      )
    );
    $public_fields['current_status'] = array(
      'property' => 'status',
      'access_callbacks' => array(
        array($this, 'accessModeration'),
      ),
      'process_callbacks' => array(
        array($this, 'getCurrentStatus'),
      )
    );
    return $public_fields;
  }
/**
   * Check permission to access moderation
   *
   * @return string
   *   ACCESS_ALLOW, ACCESS_DENY or ACCESS_IGNORE.
   */
  protected function accessModeration() {
    if (user_access('view all unpublished content')) {
      return \RestfulInterface::ACCESS_ALLOW;
    }
    return \RestfulInterface::ACCESS_DENY;
  }

  /**
   * Get entity's source.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The source URL.
   */
  protected function getEntitySource(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return $values->path['source'];
  }

  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return $values->path['alias'];
  }

  /**
   * Get entity's moderation block.
   *
   * @param integer
   *   The entity id.
   *
   * @return string
   *   The moderation block html.
   */
  protected function getModerationBlock($nid) {
    global $base_path;

    // Change the path to load the page tabs, then switch it back to prevent it from breaking other things.
    $original_path = $_GET['q'];
    if (isset($_GET['filter']['alias'])) {
      $_GET['q'] = drupal_get_normal_path($_GET['filter']['alias']);
    }
    else {
      $_GET['q'] = 'node/' . $nid;
    }
    // If viewing the draft, set the block messages to the current revision.
    if ($this->endsWith($_GET['q'], 'draft')) {
      $node = node_load($nid);
      $current_node = workbench_moderation_node_current_load($node);
      workbench_moderation_messages('view', $current_node);
    }
    else {
      workbench_moderation_messages('view');
    }
    $query_string = !empty($_SERVER['QUERY_STRING']) ? '?' . filter_xss($_SERVER['QUERY_STRING']) : '' ;

    // Our servers query starts with ?q=.  Need to remove this value.
    $query_string = str_replace('q=' . $original_path . '&', '', $query_string);
    // For some reason the & turns into amp;.  Need to remove this.
    $query_string = str_replace('amp;', '', $query_string);

    $alias_path = drupal_get_path_alias('node/' . $nid);
    $workbench_block = workbench_block_view();

    // Add target='_self' to all links to focus page reload.
    $workbench_block['content']['#markup'] = str_replace('<a href=', '<a target="_self" href=', $workbench_block['content']['#markup']);
    // Change the action path of all forms.
    $workbench_block['content']['#markup'] = str_replace('action="' . $base_path . $original_path . $query_string, 'action="' . $base_path . $_GET['q'], $workbench_block['content']['#markup']);

    // Return path to original.
    $_GET['q'] = $original_path;
    return render($workbench_block);;
  }
  /**
   * Get entity's moderation tabs.
   *
   * @param integer
   *   The entity id.
   *
   * @return array
   *   The moderation tab html.
   */
  protected function getModerationTabs($nid) {
    // Change the path to load the page tabs, then switch it back to prevent it from breaking other things.
    $original_path = $_GET['q'];
    if (isset($_GET['filter']['alias'])) {
      $_GET['q'] = drupal_get_normal_path($_GET['filter']['alias']);
    }
    else {
      $_GET['q'] = 'node/' . $nid;
    }
    $menu_tabs = menu_local_tabs();
    $_GET['q'] = $original_path;

    // Add onclick reload find and replace on all links.
    foreach ($menu_tabs['#primary'] as &$link) {
//      $link['#link']['localized_options']['attributes']['onclick'] = 'window.location.reload()';
      $link['#link']['localized_options']['attributes']['target'] = '_self';
    }
    $rendered_tabs = drupal_render($menu_tabs);
    return $rendered_tabs;
  }

  /**
   * Get entity's metatags information.
   *
   * @param integer
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $title => $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
      if ($title == 'title' && $value == $metadata['#attached']['metatag_set_preprocess_variable'][1][2]['title']) {
        $return_array['title'] = $value;
      }
    }
    if (isset($return_array['og:site_name'])) {
      // Reset the title to '[node:title] | [site:name] | University of Waterloo' from
      // '[page:current-page:title] | [site:name] | University of Waterloo'.
      $uni_name = variable_get('uw_theme_title_append', ' | University of Waterloo');
      $return_array['title'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['og:title'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['twitter:title'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['itemprop:name'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
    }
      if (isset($return_array['og:url'])) {
          // Set the url to the values listed below
          $meta_url = $wrapper->url->value();
          $return_array['og:url'] =  $meta_url;
          $return_array['twitter:url'] =  $meta_url;
          $return_array['canonical'] =  $meta_url;
      }
    return $return_array;

  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    if (static::isArrayNumeric($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->imageProcess($item);
      }
      return $output;
    }

    // Add caption from image_field_caption module if present.
    $caption = (isset($value['image_field_caption']) ? $value['image_field_caption']['value'] : NULL);

    return array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'styles' => $value['image_styles'],
      'caption' => $caption,
    );
  }

  /**
   * Overrides the path if the alias filter is valid path.
   *
   * @param string $path
   */
  public function setPath($path = '') {
    $path = implode(',', array_unique(array_filter(explode(',', $path))));
    // Cannot use $this->request as this is 2 lines lowing in the
    if (empty($path) && isset($_REQUEST['filter']['alias'])) {
      // If an alias, return node id.
      $alias = $_REQUEST['filter']['alias'];
      if ($node_path = drupal_get_normal_path($alias)) {
        $node_path = explode('/', $node_path);
        $this->path = $node_path[1];
      }
    }
    else {
      $this->path = $path;
    }
  }
  /**
   * Get entity's current status.
   *
   * @param integer
   *   The entity status.
   *
   * @return string
   *   The current status.
   */
  protected function getCurrentStatus($status) {
    if ($status) {
      return 'published';
    }
    else {
      return 'unpublished';
    }
  }

  /**
   * Override ResftulEntityBase.php to allow for references itself (parent).
   */
  protected function getTargetTypeFromEntityReference(\EntityMetadataWrapper $wrapper, $property) {
    $params = array('@property' => $property);

    if ($field = field_info_field($property)) {
      if ($field['type'] == 'entityreference') {
        return $field['settings']['target_type'];
      }
      elseif ($field['type'] == 'taxonomy_term_reference') {
        return 'taxonomy_term';
      }
      elseif ($field['type'] == 'field_collection') {
        return 'field_collection_item';
      }
      elseif ($field['type'] == 'commerce_product_reference') {
        return 'commerce_product';
      }
      elseif ($field['type'] == 'commerce_line_item_reference') {
        return 'commerce_line_item';
      }
      elseif ($field['type'] == 'node_reference') {
        return 'node';
      }

      throw new \RestfulException(format_string('Field @property is not an entity reference or taxonomy reference field.', $params));
    }
    else {
      // This is a property referencing another entity (for example, the "uid" on the node object).
      $info = $wrapper->info();
      if ($this->getEntityInfo($info['type'])) {
        return $info['type'];
      }
      // Below the 3 lines are custom.
      if (isset($info['parent']->type)) {
        return $info['parent']->type;
      }
      throw new \RestfulException(format_string('Property @property is not defined as reference in the EntityMetadataWrapper definition.', $params));
    }
  }

  /**
   * Needed until https://github.com/RESTful-Drupal/restful/pull/870 gets committed.
   */
  public function process($path = '', array $request = array(), $method = \RestfulInterface::GET, $check_rate_limit = TRUE) {
    $this->setMethod($method);
    $this->setPath($path);
    $this->setRequest($request);

    // Clear all static caches from previous requests.
    $this->staticCache->clearAll();

    // Override the range with the value in the URL.
    $this->overrideRange();

    $version = $this->getVersion();
    $this->setHttpHeaders('X-API-Version', 'v' . $version['major']  . '.' . $version['minor']);

    if (!$method_name = $this->getControllerFromPath()) {
      throw new RestfulBadRequestException('Path does not exist');
    }

    if ($check_rate_limit && $this->getRateLimitManager()) {
      // This will throw the appropriate exception if needed.
      $this->getRateLimitManager()->checkRateLimit($request);
    }

    $return = $this->{$method_name}($this->path);

    if (empty($request['__application']['rest_call'])) {
      // Switch back to the original user.
      $this->getAuthenticationManager()->switchUserBack();
    }
    return $return;
  }
  protected function endsWith($string, $test) {
    $strlen = strlen($string);
    $testlen = strlen($test);
    if ($testlen > $strlen) return false;
    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
  }
}
