<?php

/**
 * @file
 * Contains \RestfulQueryVariable.
 */

/**
 *
 */
class RestfulPublicationMetaDataResource extends \RestfulDataProviderVariable {

  /**
   * {@inheritdoc}
   */
  public function publicFieldsInfo() {
    return array();
  }

  // @code
  // /**
  //  * {@inheritdoc}
  //  */
  // public function access() {
  //   $account = $this->getAccount();
  //   return user_access('publication meta-data', $account);
  // }
  // @endcode

  /**
   * Get entity's metatags information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $title => $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
      if ($title == 'title' && $value == $metadata['#attached']['metatag_set_preprocess_variable'][1][2]['title']) {
        $return_array['title'] = $value;
      }
    }
    if (isset($return_array['og:site_name'])) {
      // Reset the title to '[node:title] | [site:name] | University of Waterloo' from
      // '[page:current-page:title] | [site:name] | University of Waterloo'.
      $uni_name = variable_get('uw_theme_title_append', ' | University of Waterloo');
      $return_array['title'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['og:title'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['twitter:title'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['itemprop:name'] = $wrapper->title->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
    }
    if (isset($return_array['og:url'])) {
      // Set the url to the values listed below
      $meta_url = $wrapper->url->value();
      $return_array['og:url'] =  $meta_url;
      $return_array['twitter:url'] =  $meta_url;
      $return_array['canonical'] =  $meta_url;
    }
    return $return_array;
  }

  /**
   * {@inheritdoc}
   */
  public function mapVariableToPublicFields($variable) {
    // Strip the $variable down to just publication settings.
    return $variable['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getVariablesForList() {
    // Get all variable start with this value.
    $starting_value = 'publication_';
    // Need to change range to the total number of variables.
    $this->setRange($this->getTotalCount());
    // Use parent funtion to grab the complete list.
    $variables = parent::getVariablesForList();
    // Go through the list an unset all of the undesirable variables.
    foreach ($variables as $index => $variable) {
      if (substr($variable['name'], 0, strlen($starting_value)) !== $starting_value) {
        unset($variables[$index]);
      }
    }
    return $variables;
  }

  /**
   * {@inheritdoc}
   */
  public function index() {
    // Get all settings/variables that start with publication_.
    $starting_value_social = 'social_';
    $variables = $this->getVariablesForList();
    // Initialize settings array so the code doesn't complain if there are no settings yet.
    $settings = array();
    foreach ($variables as $variable) {
      // Set empty values to NULL.
      if (empty($variable['value'])) {
        $variable['value'] = NULL;
      }
      $name = str_replace('publication_', '', $variable['name']);
      // Special exception for Publication Images.
      if ($name === 'faculty_or_dept_logo') {
        if ($variable['value'] && $file = file_load($variable['value'])) {
          $url = file_create_url($file->uri);
          $settings[$name] = $url;
        }
        else {
          $settings[$name] = NULL;
        }
      }
      // If social settings, then put under settings->social.
      elseif (substr($name, 0, strlen($starting_value_social)) === $starting_value_social) {
        $name = str_replace($starting_value_social, '', $name);
        $settings['social'][$name] = $variable['value'];
      }
      else {
        $settings[$name] = $variable['value'];
      }
    }
    $return_array['settings'] = $settings;

    // Return metatags from front page node.
    $frontpage = variable_get('site_frontpage', 'node/1');
    // Returns node/#.
    $normal = drupal_get_normal_path($frontpage);
    // Breaks it into node and #.
    $parts = explode('/', $normal);
    $nid = $parts[1];
    $return_array['metatags'] = $this->getMetaTags($nid);

    return $return_array;
  }

}
