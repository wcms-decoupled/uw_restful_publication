<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Meta Data'),
  'resource' => 'publication_meta_data',
  'name' => 'publication_meta_data__1_0',
  'description' => t('Expose publication meta data to the REST API.'),
  'class' => 'RestfulPublicationMetaDataResource',
  'render_cache' => array(
    'render' => TRUE,
  ),
);
