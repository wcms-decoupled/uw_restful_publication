<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationsResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['name'] = array(
      'property' => 'title_field',
    );
    $public_fields['description'] = array(
      'property' => 'body',
      'sub_property' => 'value',
    );
    $public_fields['frequency'] = array(
      'property' => 'field_publication_frequency',
    );
    $public_fields['type'] = array(
      'property' => 'field_publication_type_category',
    );
    $public_fields['current_issue'] = array(
      'property' => 'field_publication_current_issue',
      'sub_property' => 'name',
    );
    $public_fields['current_issue_id'] = array(
      'property' => 'field_publication_current_issue',
      'sub_property' => 'tid',
    );
    return $public_fields;
  }

}
