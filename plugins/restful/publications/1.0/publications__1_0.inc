<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication'),
  'resource' => 'publications',
  'name' => 'publications__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication',
  'description' => t('Export the publication content type.'),
  'class' => 'RestfulPublicationsResource',
);
