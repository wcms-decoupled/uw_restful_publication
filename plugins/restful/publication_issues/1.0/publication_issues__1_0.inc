<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Issues'),
  'resource' => 'publication_issues',
  'name' => 'publication_issues__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_issue',
  'description' => t('Export the publication issues.'),
  'class' => 'RestfulPublicationIssuesResource',
);
