<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationIssuesResource extends RestfulTaxonomyTermWithMetaTags {

  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by 'order' field in ascending order.
    return array('published' => 'DESC');
  }

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['volume'] = array(
      'property' => 'field_tax_volume',
      'resource' => array(
        'publication_volume' => 'publication_volumes',
      ),
    );
    $public_fields['issue_number'] = array(
      'property' => 'field_number',
    );
    $public_fields['published'] = array(
      'property' => 'field_publish_on',
    );
    $public_fields['cover_photo'] = array(
      'property' => 'field_cover_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'size540x370'),
    );
    $public_fields['pdf'] = array(
      'property' => 'field_file',
    );
    $public_fields['is_current_issue'] = array(
      'property' => 'name_field',
      'process_callbacks' => array(
        array($this, 'isCurrentIssue'),
      ),
    );
    return $public_fields;
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    if (static::isArrayNumeric($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->imageProcess($item);
      }
      return $output;
    }
    return array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'styles' => $value['image_styles'],
    );
  }
  /**
   * Process callback, check if the issue is the same as the current issue.
   *
   * @param array $value
   *   The current object.
   *
   * @return bool
   *   Whether or not the issue is the current issue.
   */
  protected function isCurrentIssue($value) {
    return variable_get('publication_current_issue') == $value;

  }

}
