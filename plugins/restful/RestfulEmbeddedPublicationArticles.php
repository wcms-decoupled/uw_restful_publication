<?php
/**
 * @file
 * Contains RestfulEmbeddedPublicationArticles.
 */
class RestfulEmbeddedPublicationArticles extends RestfulEntityBaseNodePublicationArticles {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['related_articles'] = array(
      'property' => 'field_related_articles',
      'sub_property' => 'nid',
    );
    $public_fields['nested_articles'] = array(
      'property' => 'field_nested_articles',
      'sub_property' => 'nid',
    );
    $public_fields['next_article'] = array(
      'property' => 'field_next_article',
      'sub_property' => 'nid',
    );
    $public_fields['previous_article'] = array(
      'property' => 'field_previous_article',
      'sub_property' => 'nid',
    );
    $public_fields['issue'] = array(
      'property' => 'field_issue',
      'sub_property' => 'name',
    );
    $public_fields['category'] = array(
      'property' => 'field_publication_category',
      'sub_property' => 'name',
    );
    // Check if the taxonomy theme field exist for publication articles.
    // Should only exist for Report and Magazine type publications.
    if (field_info_instance('node', 'field_taxonomy_theme', 'publication_article')) {
      $public_fields['theme'] = array(
        'property' => 'field_taxonomy_theme',
        'sub_property' => 'name',
      );
    }
    else {
      // Nothing to do as field is already NULL.
    }
    // $public_fields['audience'] = array(
    //  'property' => 'field_audience',
    //  'sub_property' => 'name',
    // );
    $public_fields['contains'] = array(
      'property' => 'field_publication_type',
      'sub_property' => 'name',
    );
    unset($public_fields['categories']);
    unset($public_fields['sub-navigation_articles']);

    // Unset current revision to prevent infinite loop.
    unset($public_fields['current_revision']);
    unset($public_fields['moderation_tabs']);
    unset($public_fields['moderation_block']);
    unset($public_fields['moderation_revision']);
    return $public_fields;
  }
}
