<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Issues Basic Information'),
  'resource' => 'publication_issues_basic',
  'name' => 'publication_issues_basic__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_issue',
  'description' => t('Export the publication issues basic information.'),
  'class' => 'RestfulPublicationIssuesBasicResource',
);
