<?php
/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Articles Revision'),
  'resource' => 'publication_articles_revision',
  'name' => 'publication_articles_revision__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the publication article content type.'),
  'class' => 'RestfulPublicationArticlesRevisionResource',
  // Set the authentication to "cookie".
  'authentication_types' => array(
    'cookie',
  ),
);
