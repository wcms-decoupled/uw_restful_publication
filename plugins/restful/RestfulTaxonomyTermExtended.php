<?php
/**
 * @file
 * Extends the RestfulEntityBaseTaxonomyTerm class.
 */
class RestfulTaxonomyTermExtended extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );
    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );
    $public_fields['description'] = array(
      'property' => 'description_field',
      'sub_property' => 'value',
    );
    $public_fields['name'] = array(
      'property' => 'name_field',
    );
    $public_fields['machine_name'] = array(
      'property' => 'machine_name',
    );
    return $public_fields;
  }
  /**
   * Get entity's source.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The source URL.
   */
  protected function getEntitySource(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return 'taxonomy/term/' . $values->tid;
  }
  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return drupal_lookup_path('alias', 'taxonomy/term/' . $values->tid);
  }

}
