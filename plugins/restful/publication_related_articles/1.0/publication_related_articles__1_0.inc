<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Related Articles'),
  'resource' => 'publication_related_articles',
  'name' => 'publication_related_articles__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the publication related articles.'),
  'class' => 'RestfulPublicationRelatedArticlesResource',
);
