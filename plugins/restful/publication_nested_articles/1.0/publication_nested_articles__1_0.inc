<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Nested Articles'),
  'resource' => 'publication_nested_articles',
  'name' => 'publication_nested_articles__1_0',
  'entity_type' => 'node',
  'bundle' => 'nested_publication_articles',
  'description' => t('Export the publication article content type.'),
  'class' => 'RestfulPublicationNestedArticlesResource',
);
