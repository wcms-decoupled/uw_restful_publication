<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Next Articles'),
  'resource' => 'publication_next_articles',
  'name' => 'publication_next_articles__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the publication next articles.'),
  'class' => 'RestfulPublicationNextArticlesResource',
);
