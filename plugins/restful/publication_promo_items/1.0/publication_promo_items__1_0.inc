<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Promotional Items'),
  'resource' => 'publication_promo_items',
  'name' => 'publication_promo_items__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_promotional_item',
  'description' => t('Export the publication promotional items content type.'),
  'class' => 'RestfulPublicationPromoItemsResource',
);
