<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationPromoItemsResource extends RestfulEntityBaseNode {

  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by 'order' field in ascending order.
    // Note: Removed until reordering is fixed.
    // return array('order' => 'ASC');
  }
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    // Note: Remove until reordering is fixed.
    // $public_fields['order'] = array(
    //  'property' => 'field_promo_order',
    // );
    $public_fields['heading'] = array(
      'property' => 'title_field',
    );
    $public_fields['heading_hidden'] = array(
      'property' => 'field_do_not_display_title',
    );
    $public_fields['categories'] = array(
      'property' => 'field_promo_categories',
    );
    $public_fields['image'] = array(
      'property' => 'field_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'width300'),
    );
    $public_fields['background_image'] = array(
      'property' => 'field_background_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'width300'),
    );
    $public_fields['content'] = array(
      'property' => 'field_body_no_summary',
      'sub_property' => 'value',
    );
    $public_fields['file'] = array(
      'property' => 'field_file',
    );
    // TODO: make visibility field private.
    $public_fields['visibility'] = array(
      'property' => 'field_block_visibility',
    );
    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    return $public_fields;
  }

  /**
   * Get entity's metatags information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
    }
    return $return_array;
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    if (static::isArrayNumeric($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->imageProcess($item);
      }
      return $output;
    }
    return array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'styles' => $value['image_styles'],
    );
  }

  /**
   * Overrides parent viewEntity.
   */
  public function viewEntity($id) {
    global $base_path;
    $request = $this->getRequest();
    $entity_id = $this->getEntityIdByFieldId($id);
    if (!$this->isValidEntity('view', $entity_id)) {
      return;
    }
    $wrapper = entity_metadata_wrapper($this->entityType, $entity_id);
    $wrapper->language($this->getLangCode());
    // Check if there is a visibility filter.
    if (isset($request['visibility'])) {
      $visibility_path = $request['visibility'];
      // If filter path does not match the path, then return.
      // if visibility pattern is not set, then show everywhere by default.
      $visibility_pattern = $wrapper->field_block_visibility->value();

      // If you need to remove the $base_path from the beginning of path uncomment the line below.
      // @todo This is probably not the best way to do this.
      // @code
      // $visibility_path = preg_replace('/^' . str_replace('/', '\/', $base_path) . '/', '', $visibility_path);
      // @endcode
      if (isset($visibility_pattern) && !drupal_match_path($visibility_path, $visibility_pattern)) {
        return;
      }
    }
    return parent::viewEntity($id);
  }

}
