<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Articles Teaser'),
  'resource' => 'publication_articles_teaser',
  'name' => 'publication_articles_teaser__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the teaser information for publication article content type.'),
  'class' => 'RestfulPublicationArticlesTeaserResource',
);
