<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationArticlesTeaserResource extends RestfulEntityBaseNode {
  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by 'order' field in ascending order.
    return array('order' => 'ASC');
  }

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['order'] = array(
      'property' => 'field_order',
    );
    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );
    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );
    $public_fields['headline'] = array(
      'property' => 'title_field',
    );
    $public_fields['sub_headline'] = array(
      'property' => 'field_publication_sub_headline',
    );
    $public_fields['teaser'] = array(
      'property' => 'field_publication_teaser',
    );
    $public_fields['author'] = array(
      'property' => 'field_author_collection',
      'sub_property' => 'field_author_name',
    );
    $public_fields['issue'] = array(
      'property' => 'field_issue',
      'resource' => array(
        'publication_issue' => 'publication_issues_basic',
      ),
    );
    $public_fields['category'] = array(
      'property' => 'field_publication_category',
      'sub_property' => 'name',
    );
    $public_fields['categories'] = array(
      'property' => 'field_publication_category',
      'resource' => array(
        'publication_categories' => 'publication_categories_no_metadata',
      ),
    );
    // Check if the taxonomy theme field exist for publication articles.
    // Should only exist for Report and Magazine type publications.
    if (field_info_instance('node', 'field_taxonomy_theme', 'publication_article')) {
      $public_fields['themes'] = array(
        'property' => 'field_taxonomy_theme',
        'resource' => array(
          'uwaterloo_theme' => 'publication_themes_no_metadata',
        ),
      );
    }
    else {
      // Do not remove field.  Return NULL.
      $public_fields['themes'] = array(
        'callback' => array($this, 'returnNull'),
      );
    }
    $public_fields['audiences'] = array(
      'property' => 'field_audience',
      'sub_property' => 'name',
    );
    $public_fields['contains'] = array(
      'property' => 'field_publication_type',
      'resource' => array(
        'publication_type' => 'publication_types_no_metadata',
      ),
    );
    $public_fields['image_position'] = array(
      'property' => 'field_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original'),
    );
    $public_fields['feature_image'] = array(
      'property' => 'field_feature_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'medium', 'large', 'size540x370', 'cover_photo', 'cover_photo_x2'),
    );
    $public_fields['in_current_issue'] = array(
      'property' => 'field_issue',
      'sub_property' => 'name',
      'process_callbacks' => array(
        array($this, 'inCurrentIssue'),
      ),
    );

    return $public_fields;
  }

  /**
   * Return NULL so public field still displays in API.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return NULL
   */
  protected function returnNull(\EntityDrupalWrapper $wrapper) {
    return NULL;
  }

  /**
   * Get entity's source.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The source URL.
   */
  protected function getEntitySource(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return $values->path['source'];
  }
  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return $values->path['alias'];
  }
  /**
   * Get entity's metatags information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
    }
    return $return_array;
  }
  /**
   * Process callback, check if the issue is the same as the current issue.
   *
   * @param array $value
   *   The current object.
   *
   * @return bool
   *   Whether or not the issue is the current issue.
   */
  protected function inCurrentIssue($value) {
    return variable_get('publication_current_issue') == $value;

  }
  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    if (static::isArrayNumeric($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->imageProcess($item);
      }
      return $output;
    }

    // Add caption from image_field_caption module if present.
    $caption = (isset($value['image_field_caption']) ? $value['image_field_caption']['value'] : NULL);

    return array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'styles' => $value['image_styles'],
      'caption' => $caption,
    );
  }

  /**
   * Overrides parent filter the query for list.
   *
   * @param \entityfieldquery $query
   *   The query object.
   *
   * @see \restfulentitybase::getqueryforlist
   */
  protected function queryforlistfilter(\entityfieldquery $query) {
    foreach ($this->parserequestforlistfilter() as $filter) {
      // Map alias to the node id.
      if ($filter['public_field'] == 'alias') {
        $node_path = explode('/', drupal_get_normal_path($filter['value'][0]));
        $nid = $node_path[1];
        $query->entityCondition('entity_id', $nid, $filter['operator'][0]);
        // Unset the alias filter to prevent problems when we call parent function.
        unset($this->request['filter']['alias']);
      }
      elseif ($filter['public_field'] == 'issue') {
        // Issue is the url friend machine name.  Replace it with its tid.
        $request = $this->getRequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_issue');
        $request['filter']['issue'] = $term->tid;
        $this->setRequest($request);
      }
      elseif ($filter['public_field'] == 'categories') {
        // Category is the url friend machine name.  Replace it with its tid.
        $request = $this->getRequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_categories');
        $request['filter']['categories'] = $term->tid;
        $this->setRequest($request);
      }
      elseif ($filter['public_field'] == 'themes') {
        // Category is the url friend machine name.  Replace it with its tid.
        $request = $this->getRequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'uwaterloo_theme');
        $request['filter']['themes'] = $term->tid;
        $this->setRequest($request);
      }
      elseif ($filter['public_field'] == 'contains') {
        // Category is the url friend machine name.  Replace it with its tid.
        $request = $this->getRequest();
        $term = taxonomy_term_machine_name_load(str_replace('-', '_', $filter['value'][0]), 'publication_type');
        $request['filter']['contains'] = $term->tid;
        $this->setRequest($request);
      }
      elseif ($filter['public_field'] == 'in_current_issue' && $filter['value'][0] == 'true') {
        // Set the filter to the current issue tid.
        $request = $this->getRequest();
        $current_issue_name = variable_get('publication_current_issue');
        $term = taxonomy_get_term_by_name($current_issue_name, 'publication_issue');
        $request['filter']['in_current_issue'] = array_shift($term)->tid;
        $this->setRequest($request);
      }
    }
    parent::queryforlistfilter($query);;
  }

}
