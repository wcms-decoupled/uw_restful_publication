<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Articles'),
  'resource' => 'publication_articles',
  'name' => 'publication_articles__1_0',
  'entity_type' => 'node',
  'bundle' => 'publication_article',
  'description' => t('Export the publication article content type.'),
  'class' => 'RestfulPublicationArticlesResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
