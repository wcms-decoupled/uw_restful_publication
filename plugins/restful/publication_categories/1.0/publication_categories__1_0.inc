<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Categories'),
  'resource' => 'publication_categories',
  'name' => 'publication_categories__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_categories',
  'description' => t('Export the publication categories.'),
  'class' => 'RestfulPublicationCategoriesResource',
);
