<?php

/**
 * @file
 */

/**
 *
 */
class RestfulPublicationVolumesResource extends RestfulTaxonomyTermWithMetaTags {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['number'] = array(
      'property' => 'field_number',
    );
    return $public_fields;
  }

}
