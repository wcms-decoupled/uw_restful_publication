<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Volumes'),
  'resource' => 'publication_volumes',
  'name' => 'publication_volumes__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'publication_volume',
  'description' => t('Export the publication volumes.'),
  'class' => 'RestfulPublicationVolumesResource',
);
