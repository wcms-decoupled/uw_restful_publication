<?php
/**
 * @file
 */
$plugin = array(
  'label' => t('Determine Node Bundle'),
  'resource' => 'determine_bundle',
  'name' => 'determine_bundle__1_0',
  'entity_type' => 'node',
  'description' => t('Export the node bundle.'),
  'class' => 'RestfulDetermineBundle',
  // Set the authentication to "cookie".
  'authentication_types' => array(
    'cookie',
  ),
);
