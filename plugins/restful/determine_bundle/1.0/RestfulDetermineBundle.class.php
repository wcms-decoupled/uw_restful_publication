<?php
/**
 * @file
 */
class RestfulDetermineBundle extends RestfulEntityBaseNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['bundle'] = array(
      'property' => 'type',
    );
    return $public_fields;
  }
}
