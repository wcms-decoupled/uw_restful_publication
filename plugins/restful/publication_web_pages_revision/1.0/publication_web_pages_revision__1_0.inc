<?php
/**
 * @file
 */
$plugin = array(
  'label' => t('Publication Web Pages Revision'),
  'resource' => 'publication_web_pages_revision',
  'name' => 'publication_web_pages_revision__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_web_page',
  'description' => t('Export the publication web pages content type revision.'),
  'class' => 'RestfulPublicationWebPagesRevisionResource',
  // Set the authentication to "cookie".
  'authentication_types' => array(
    'cookie',
  ),
);
