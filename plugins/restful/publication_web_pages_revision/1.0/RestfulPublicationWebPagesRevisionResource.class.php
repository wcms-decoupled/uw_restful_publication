<?php

/**
 * @file
 */
class RestfulPublicationWebPagesRevisionResource extends RestfulPublicationWebPagesResource {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();


    // Unset current revision to prevent infinite loop.
    unset($public_fields['moderation_revision']);

    $public_fields['revision_state'] = array(
      'property' => 'nid',
      'access_callbacks' => array(
        array($this, 'accessModeration'),
      ),
      'process_callbacks' => array(
        array($this, 'getRevisionState'),
      )
    );
    return $public_fields;
  }

  /**
   * Overrides RestfulEntityBase:viewEntity().
   * @param $id
   * @return array|null
   */
  public function viewEntity($id) {
    $resource = 'publication_web_pages_revision';
    $entity = entity_load_single('node', $id);
    if ($entity->vid == $entity->workbench_moderation['current']->vid && $entity->workbench_moderation['current']->state != 'draft') {
      return;
    }
    else {
      $current_vid = $entity->workbench_moderation['current']->vid;
      $handler = restful_get_restful_handler($resource);
      $request = array(
        'id' => $current_vid,
      );

      $revision = $this->viewEntityRevision(entity_metadata_wrapper('node', entity_revision_load('node', $entity->workbench_moderation['current']->vid)));
      return $revision;
    }
  }

  /**
   *  View the entity's revision.
   *
   * @param EntityMetadataWrapper $wrapper
   * @return array
   * @throws RestfulException
   * @throws RestfulServerConfigurationException
   */
  public function viewEntityRevision(\EntityMetadataWrapper $wrapper) {
    $wrapper->language($this->getLangCode());
    $values = array();

    $limit_fields = !empty($request['fields']) ? explode(',', $request['fields']) : array();

    foreach ($this->getPublicFields() as $public_field_name => $info) {
      if ($limit_fields && !in_array($public_field_name, $limit_fields)) {
        // Limit fields doesn't include this property.
        continue;
      }

      $value = NULL;

      if ($info['create_or_update_passthrough']) {
        // The public field is a dummy one, meant only for passing data upon
        // create or update.
        continue;
      }

      if ($info['callback']) {
        $value = static::executeCallback($info['callback'], array($wrapper));
      }
      else {
        // Exposing an entity field.
        $property = $info['property'];
        $sub_wrapper = $info['wrapper_method_on_entity'] ? $wrapper : $wrapper->{$property};

        // Check user has access to the property.
        if ($property && !$this->checkPropertyAccess('view', $public_field_name, $sub_wrapper, $wrapper)) {
          continue;
        }

        if (empty($info['formatter'])) {
          if ($sub_wrapper instanceof EntityListWrapper) {
            // Multiple values.
            foreach ($sub_wrapper as $item_wrapper) {
              $value[] = $this->getValueFromProperty($wrapper, $item_wrapper, $info, $public_field_name);
            }
          }
          else {
            // Single value.
            $value = $this->getValueFromProperty($wrapper, $sub_wrapper, $info, $public_field_name);
          }
        }
        else {
          // Get value from field formatter.
          $value = $this->getValueFromFieldFormatter($wrapper, $sub_wrapper, $info);
        }
      }

      if (isset($value) && $info['process_callbacks']) {
        foreach ($info['process_callbacks'] as $process_callback) {
          $value = static::executeCallback($process_callback, array($value));
        }
      }

      $values[$public_field_name] = $value;
    }

    $this->setRenderedCache($values, $this->getEntityCacheTags($entity_id));
    return $values;
  }
  /**
   * Get entity's current state.
   *
   * @param integer
   *   The entity id.
   *
   * @return string
   *   The current state.
   */
  protected function getRevisionState($nid) {
    $entity = entity_load_single('node', $nid);
    $current_revision_state = $entity->workbench_moderation['current']->state;
    return $current_revision_state;
  }
}

