<?php
/**
 * @file
 * Contains RestfulTaxonomyTermWithMetaTags.
 */
class RestfulTaxonomyTermWithMetaTags extends RestfulTaxonomyTermExtended {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['metatags'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    return $public_fields;
  }

  /**
   * Get entity's metatags information.
   *
   * @param int $tid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($tid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('taxonomy_term', $tid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'taxonomy_term');
    foreach ($metadata_array as $title => $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
      if ($title == 'title' && $value == $metadata['#attached']['metatag_set_preprocess_variable'][1][2]['title']) {
        $return_array['title'] = $value;
      }
    }
    if (isset($return_array['og:site_name'])) {
      // Reset the title to '[node:title] | [site:name] | University of Waterloo' from
      // '[page:current-page:title] | [site:name] | University of Waterloo'.
      $uni_name = variable_get('uw_theme_title_append', ' | University of Waterloo');
      $return_array['title'] = $wrapper->name->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['og:title'] = $wrapper->name->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['twitter:title'] = $wrapper->name->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
      $return_array['itemprop:name'] = $wrapper->name->value() . ' | ' . $return_array['og:site_name'] . $uni_name;
    }
    if (isset($return_array['og:url'])) {
      // Set the url to the values listed below
      $meta_url = $wrapper->url->value();
      $return_array['og:url'] =  $meta_url;
      $return_array['twitter:url'] =  $meta_url;
      $return_array['canonical'] =  $meta_url;
    }
    return $return_array;
  }

}
