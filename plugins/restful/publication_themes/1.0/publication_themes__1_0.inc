<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Publication Themes'),
  'resource' => 'publication_themes',
  'name' => 'publication_themes__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uwaterloo_theme',
  'description' => t('Export the publication themes.'),
  'class' => 'RestfulPublicationThemesResource',
);
