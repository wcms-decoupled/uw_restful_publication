Purpose
=======

This module builds custom endpoints for publication website. All of the endpoints are public, as there is not private information being sent. This may change in the future.  

More about this module
-----------------------
- Depends on https://github.com/RESTful-Drupal/restful 1.x
- The endpoints are all push,  there is no pulling (as of right now)
- No authentication
- Endpoints for nodes and menus
- Custom class for making menu end points, which depends on https://www.drupal.org/project/entity_menu_links to turn menus into entities
  - There is quirks about this dependency module, as it does not turn 'system' menu items into entities (which include one added through views ui).
-  Multiple endpoints for the same type of nodes.  Some are richer in content than others.  
  - One endpoint is 1.7Mb for all data, while a trimmed one is only 110Kb.

Versioning
-----------

RESTFul module allows easy versioning.  For the WCMS, currently we are not using it (just using 1.0 for everything).  When we roll out a new release, we control the front-end and back-end of all sites using the API, so we can make changes to both at the same time.  This will probably change in the future.  Maybe after Publication 1.0 is released.

Other Notes
-----------

When creating a new endpoint (or pulling it down on a production server), you need to clear cache twice. This is a problem with Drupal and CTools caching all known classes before caching new plugins.
